# README #

Este es el repositorio correspodiente al código del trabajo fin de grado (TFG)
CLúster de Raspberry Pi para la reproducción sincronizada de múltiples señales
de vídeo y comandos domóticos.

Autor: Gonzalo Matarrubia González
ETSIDI - Universidad Politécnica de Madrid

Todo el código contenido en este repositorio está protegido bajo la licencia
de software libre GNU General Public License (GLP) 3.0
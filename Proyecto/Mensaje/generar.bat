@echo off

:: Genera las clases para el Cliente_py y el Servidor_rbpi

protoc -I=. --python_out=../Cliente_py mensaje.proto
protoc -I=. --cpp_out=../Servidor_rbpi mensaje.proto

echo "Se han generado los archivos mensaje.pb.h y mensaje.pb.cc"

pause
exit

/***********************************************************************
 * Archivo: vlc_object.cpp                                             *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster rasberry pi hecho por Gonzalo     *
 * Matarrubia.                                                         *
 **********************************************************************/
#ifndef SERVIDOR_RBPI_GESTORMP_H
#define SERVIDOR_RBPI_GESTORMP_H

#include "vlc_object.h"
#include "conn_zmq.h"
#include "mensaje.pb.h"

/*La clase gestora es la encargada de unir las comunicaciones
 * del socket de escucha con las acciones correspondientes en
 * la parte de reproducción de vídeo VLC */
class gestorMP {

protected:
    vlc_object *v;
    conn_zmq *z;
public:
    gestorMP(vlc_object *_v, conn_zmq *_z);
    void gestion_recv_cmd();
    void preparar_envio_paths();
    void cargar_video();
};

#endif //SERVIDOR_RBPI_GESTORMP_H
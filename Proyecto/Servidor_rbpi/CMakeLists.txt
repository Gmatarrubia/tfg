cmake_minimum_required(VERSION 3.6)
project(Servidor_rbpi)

set(CMAKE_CXX_STANDARD 11)
SET(CMAKE_MODULE_PATH  ${CMAKE_SOURCE_DIR}/cmake
                        "C:/Program\ Files/protobuf/cmake/")

include(FindProtobuf)
find_package(Threads)
find_package(ZMQ REQUIRED)
find_package(LIBVLC REQUIRED)
find_package(Protobuf REQUIRED)
include_directories(${LIBVLC_INCLUDE_DIR})
include_directories (${ZMQ_INCLUDE_DIR})
include_directories (${PROTOBUF_INCLUDE_DIR})

if (${CMAKE_SYSTEM_PROCESSOR} STREQUAL "armv7l")
    message(STATUS "Compilando con configuracion: RPI v3")
    add_definitions(-DRPI3)
endif ()

set(SOURCE_FILES main.cpp vlc_object.cpp conn_zmq.cpp mensaje.pb.cc gestorMP.cpp latidos_zmq.cpp lst_medias.cpp setIP.h zmqvideo.cpp)
set(HEADER_FILES conn_zmq.h vlc_object.h mensaje.pb.h gestorMP.h latidos_zmq.h lst_medias.h rpiGPIO.h setIP.h zmqvideo.h)
add_executable(Servidor_rbpi ${SOURCE_FILES})
target_link_libraries(Servidor_rbpi ${LIBVLC_LIBRARY} ${ZMQ_LIBRARY} ${PROTOBUF_LIBRARY} ${CMAKE_THREAD_LIBS_INIT})
/***********************************************************************
 * Archivo: setIP.h                                                    *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 **********************************************************************/

#include <iostream>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#ifndef SERVIDOR_RBPI_SETIP_H
#define SERVIDOR_RBPI_SETIP_H

using namespace std;
void setIP(string IP, string iface = "eth0") {

    struct ifreq ifr;
    struct sockaddr_in sin;
    int s;

    s = socket(AF_INET, SOCK_DGRAM, 0); //temporary socket

    memset(&ifr, 0x0, sizeof(struct ifreq));
    memset(&sin, 0x0, sizeof(struct sockaddr));
    strncpy(ifr.ifr_name, iface.c_str(), IF_NAMESIZE - 1);
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = inet_addr(IP.c_str()); //Establecemos la IP
    memcpy(&ifr.ifr_addr, &sin, sizeof(struct sockaddr));

    if (ioctl(s, SIOCSIFADDR, &ifr) < 0) { //set IP-Adress and check for errors
        close(s);
    }
    return;
}
#endif //SERVIDOR_RBPI_SETIP_H

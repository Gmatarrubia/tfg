/***********************************************************************
 * Archivo: vlc_object.cpp                                             *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 **********************************************************************/

#include "vlc_object.h"
#include <sstream>

//Se crea una lista de los vídeos disponibles
vlc_object::vlc_object ( ) : myvideos("/home/VLC/videos/"){
    //Cargar listas de archivos
    index = myvideos.cargarlista();
    playing = 0;
}

//Se crea un "contexto" de las herramientas de VLC
void vlc_object::new_engine(int npath,
                            int _izq, int _arriba, int _dcha, int _abajo,
                            int enblAudio, int _rot) {
    //Si había una instancia anterior, se libera primero
    if (inst!=NULL)release_vlc_inst();
    //Argumentos iniciales, comentar o descomentar a placer
    int i = 0;
    char *vlc_argv[25];
    vlc_argv[i] = (char *)"-I dummy";                       i++;
    vlc_argv[i] = (char *)"--no-osd";                       i++;
    //vlc_argv[i] = (char *)"--config=/home/gonza/.config/vlc/vlcrc";  i++;
    vlc_argv[i] = (char *)"--aout=alsa";                    i++;
#ifdef RPI3
    vlc_argv[i] = (char *)"--vout=omxil_vout";              i++;
#endif
    if (!enblAudio) {
        vlc_argv[i] = (char *)"--no-audio";                 i++;
    }
    //Aplicamos los recortes corespondientes
    if (_izq || _arriba || _dcha || _abajo) {
        vlc_argv[i] = (char *) "--video-filter=croppadd";   i++;
    }
    if (_izq){
        stringstream cropVar;
        cropVar << _izq;
        izq = "--croppadd-cropleft=" + cropVar.str();
        vlc_argv[i] = (char *) izq.c_str();                 i++;
    }
    if (_arriba){
        stringstream cropVar;
        cropVar << _arriba;
        arriba = "--croppadd-croptop=" + cropVar.str();
        vlc_argv[i] = (char *) arriba.c_str();              i++;
    }
    if (_dcha){
        stringstream cropVar;
        cropVar << _dcha;
        dcha = "--croppadd-cropright=" + cropVar.str();
        vlc_argv[i] = (char *) dcha.c_str();                i++;
    }
    if (_abajo){
        stringstream cropVar;
        cropVar << _abajo;
        abajo = "--croppadd-cropbottom=" + cropVar.str();
        vlc_argv[i] = (char *) abajo.c_str();               i++;
    }
    //vlc_argv[i] = (char *) "--video-splitter=wall";         i++;
    //vlc_argv[i] = (char *)"--wall-rows=3";                  i++;
    //vlc_argv[i] = (char *)"--wall-cols=3";                  i++;
    //vlc_argv[i] = (char *)"--wall-active=5";                i++;
    //************************************************************
    //vlc_argv[i] = (char *) "--video-filter=croppadd";       i++;
    //vlc_argv[i] = (char *)"--croppadd-cropright=250";       i++;
    //vlc_argv[i] = (char *)"--croppadd-croptop=109";         i++;
    //vlc_argv[i] = (char *)"--croppadd-cropleft=309";        i++;
    //vlc_argv[i] = (char *)"--croppadd-cropbottom=309";      i++;
    if (_rot > 0) {
        vlc_argv[i] = (char *) "--video-filter=rotate";     i++;
        stringstream srot;
        srot << _rot;
        rot = "--rotate-angle=" + srot.str();
        vlc_argv[i] = (char *) rot.c_str();           i++;
    }
    //vlc_argv[i] = (char *)"--no-video-title-show";          i++;
    //vlc_argv[i] = (char *)"--ignore-config";                i++;
    //vlc_argv[i] = (char *)"--video-filter=transform";       i++;
    //vlc_argv[i] = (char *)"--transform-type=180";           i++;
    //vlc_argv[i] = (char *)"--x11-display :0";               i++;

    /* Pasamos el numero y los argumentos iniciales */
    inst = libvlc_new (i, vlc_argv );

    new_vlc_mp(npath);
 }

//Apunta a un archivo y crea un media player donde reproducirlo
 void vlc_object::new_vlc_mp(int npath){
    /* Se fija el video al que apuntar */
    if( 0 <= npath && npath <= index){
        m = libvlc_media_new_path (inst, myvideos.path[npath].c_str());
    }
    //Si había un mp anterior, se libera primero
    if (mp!=NULL)release_mp();
    /* Se crea un media player con el media definido*/
    mp = libvlc_media_player_new_from_media (m);
    /*while (libvlc_media_get_parsed_status(m) != libvlc_media_parsed_status_done) {
        libvlc_media_parse_with_options(m, libvlc_media_parse_local, 0);
    }
    libvlc_video_get_size(mp, 0, &sizeX, &sizeY);
    std::cout << sizeX << " : " << sizeY << std::endl;
    */
    release_vlc_m(); //Se libera el puntero m

    libvlc_set_fullscreen(mp, 1);
}

//Libera el puntero que apuntaba al vídeo
void vlc_object::release_vlc_m(){     
    /* No need to keep the media now */
    libvlc_media_release (m);
}

/////////////////////////////////////////////////////////////////
//De aquí para abajo son métodos de gestión del archivo media  //
//tales como pausar, reanudar reprodución, etc. //
/////////////////////////////////////////////////////////////////
//Selecciona la ventana en la que reproducir el vídeo
void vlc_object::set_xwindow(){
    /* if we have a window around */
    libvlc_media_player_set_xwindow (mp,0);
}

void vlc_object::Play(){
    /* play the media_player */
    libvlc_media_player_play (mp);
}

int vlc_object::IsPlaying(){
    return libvlc_media_player_is_playing(mp);
}

void vlc_object::ResumePause() {
    libvlc_media_player_pause(mp);
}

void vlc_object::Stop(){
   /* Stop playing */
   libvlc_media_player_stop (mp);
}

void vlc_object::release_mp(){
     /* Free the media_player */
     libvlc_media_player_release (mp);
}

void vlc_object::release_vlc_inst(){
    libvlc_release (inst);
}
int vlc_object::get_length(){
    len = libvlc_media_player_get_length(mp);
    return len;
}

int vlc_object::get_state(){
    //sleep(0.1);
    if (mp==NULL) return 1;
    return libvlc_media_player_get_state(mp);
}

vlc_object::~vlc_object (){
	libvlc_media_player_release (mp);
	release_vlc_inst();
}

int vlc_object::get_index() {
    return index;
}

/***********************************************************************
 * Archivo: zmqvideo.cpp                                               *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 **********************************************************************/

#include <string>
#include <iostream>
#include <unistd.h>
#include <sstream>
#include "zmqvideo.h"
#include "mensaje.pb.h"

//Conecta el socket y crea el thread
void zmqVideo::recv_videos_thread() {
    bind("tcp://*:2324");
    pthread_create(&wk_thread, NULL, &thread, this);
}
//Funcion estática para poder crear un thread que apunte a un método
void* zmqVideo::thread(void* arg) {
    return static_cast<zmqVideo*>(arg)->recv_videos();
}

//Funcíon de ejecución continua que envía un latido cada 5 segundos.
void* zmqVideo::recv_videos() {
    for(int i=0 ; i<8; i){ //bucle infinito
        recv_data();
    }
    return this;
}

void zmqVideo::recv_data( ){
    message_t in;
    Video video_in;
    //Esperamos respuesta
    s.recv (&in);
    std::string msg_str(static_cast<char*>(in.data()), in.size());
    video_in.ParseFromString(msg_str);
    std::string path = "/home/gonza/videos/" + video_in.path();
    FILE *video_file;
    video_file = fopen(path.c_str(), "w+");
    s.send (in);
    while(true){
        message_t data(512);
        int valor;
        size_t len = sizeof(valor);
        //Recibir y procesar
        s.recv(&data);
        fwrite(data.data(), data.size(), 1, video_file);
        s.getsockopt(ZMQ_RCVMORE, &valor, &len );
        if (!valor) break;
    }
    //TODO:Habria que cargar la lista de video de nuevo
    s.send (in);
    fclose(video_file);
}
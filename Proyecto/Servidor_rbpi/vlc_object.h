/***********************************************************************
 * Archivo: vlc_object.h                                               *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 **********************************************************************/

#ifndef SERVIDOR_RBPI_VLC_OBJECT_H
#define SERVIDOR_RBPI_VLC_OBJECT_H

#include <vlc/vlc.h>
#include <iostream>
#include "lst_medias.h"

/* La clase VLC_OBJECT se puede considerar un Binding propio
 * de la API de libvlc escrita en C++ de esta manera podemos
 * gestionar el vídeo con POO */
class vlc_object {
	private:

	libvlc_instance_t *inst=NULL;
    libvlc_media_player_t *mp=NULL;
    libvlc_media_t *m=NULL;

    static char *vlc_argv[];
    int vlc_argc;
    int index;
    int playing;
    int recortar = 0;
    std::string izq, arriba, dcha, abajo, rot;
    std::string geometry;
    unsigned int sizeX, sizeY;
    libvlc_time_t len;
    
    public:

    lst_medias myvideos;
    void new_engine(int npath,
					int _xfrom, int _yfrom, int _xto, int _yto,
                    int enblAudio, int rot);
    void new_vlc_mp(int npath);
    void release_vlc_m();
    void set_xwindow();
    void Play();
    int  IsPlaying();
    void ResumePause();
    void Stop();
    void release_mp();
    void release_vlc_inst();
	int get_length();
    int get_state();
    int get_index();
    vlc_object();
    ~vlc_object();
    
    
};

#endif //SERVIDOR_RBPI_VLC_OBJECT_H
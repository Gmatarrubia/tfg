# - Try to find ZMQ
# Once done this will define
# ZMQ_FOUND - System has ZMQ
# ZMQ_INCLUDE_DIRS - The ZMQ include directories
# ZMQ_LIBRARIES - The libraries needed to use ZMQ
# ZMQ_DEFINITIONS - Compiler switches required for using ZMQ

#Para que encuentre la librería .dll en windows
SET(CMAKE_FIND_LIBRARY_SUFFIXES ".lib" ".dll" ".so" ".a")

find_path ( ZMQ_INCLUDE_DIR NAMES zmq.h PATHS
    "/usr/include"
    "/usr/local/include"
    "C:/Program\ Files/ZeroMQ/include/")
find_library ( ZMQ_LIBRARY NAMES zmq PATHS
    "/usr/lib/"
    "/usr/lib/local/"
    "c:/Program\ Files/ZeroMQ/bin/")

set ( ZMQ_LIBRARIES ${ZMQ_LIBRARY} )
set ( ZMQ_INCLUDE_DIRS ${ZMQ_INCLUDE_DIR} )

include ( FindPackageHandleStandardArgs )
# handle the QUIETLY and REQUIRED arguments and set ZMQ_FOUND to TRUE
# if all listed variables are TRUE
IF (ZMQ_INCLUDE_DIR AND ZMQ_LIBRARY)
   SET(ZMQ_FOUND TRUE)
ENDIF (ZMQ_INCLUDE_DIR AND ZMQ_LIBRARY)

IF (ZMQ_FOUND)
   IF (NOT ZMQ_FIND_QUIETLY)
      MESSAGE(STATUS "Found ZMQ include-dir path: ${ZMQ_INCLUDE_DIR}")
      MESSAGE(STATUS "Found ZMQ library path:${ZMQ_LIBRARY}")
   ENDIF (NOT ZMQ_FIND_QUIETLY)
ELSE (ZMQ_FOUND)
   IF (ZMQ_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find ZMQ")
   ENDIF (ZMQ_FIND_REQUIRED)
ENDIF (ZMQ_FOUND)

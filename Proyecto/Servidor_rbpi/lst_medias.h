/***********************************************************************
 * Archivo: lst_medias.h                                               *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 **********************************************************************/

#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <cstring>
#include <vlc/vlc.h>
#include <iostream>

using namespace std;

class lst_medias {
protected:
    string dir;
protected:
public:
    //TODO: No limitar a 30 videos
    string nombre[30];
    string path[30];
    lst_medias(string _dir);
    int cargarlista( );
};
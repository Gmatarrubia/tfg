/***********************************************************************
 * Archivo: main.cpp                                                   *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 **********************************************************************/
 
#include <iostream>
#include "conn_zmq.h"
#include "latidos_zmq.h"
#include "vlc_object.h"
#include "gestorMP.h"
#include "zmqvideo.h"

#ifdef RPI3
#include "rpiGPIO.h"
#include "setIP.h"
#endif

#define IP_BROKER "tcp://192.168.1.40:2322"
//TODO: Subir a user VLC y cambiar crontab de root
int main(int argc, char* argv[])
{
    std::string IP;
#ifdef RPI3
    IP = getIPrpiGPIO();
    setIP(IP, "eth0");
#else
    // IP = argv[0]; //Para pasarlo por parametro
    IP = "192.168.1.43"; //Hardcodeado
#endif
    //Declaración de vlc_object, socket y thread
    vlc_object myVlc;
    conn_zmq myZmq(ZMQ_REP);
    latidos_zmq latido(IP_BROKER, IP, &myVlc);
    zmqVideo myzmqvideo;

    //Se lanza thread de latidos, el servidor y el gestor de comandos
    //Ademas lanza thread de descarga de videos
    latido.send_latidos_thread();
    myZmq.bind("tcp://*:2323");
    myzmqvideo.recv_videos_thread();
    gestorMP mygestor(&myVlc,&myZmq);

    //Se genera una primera ventana VLC con un video en play
    myVlc.new_engine(1,0,0,0,0,1,0);
    //myVlc.set_xwindow();
    myVlc.Play();
    mygestor.preparar_envio_paths();

    //Escucha constante de comandos con respuesta
    for(int i=0; i<8; i){ //bucle infinito
        mygestor.gestion_recv_cmd();
    }

    myVlc.Stop();
    return 0;
}

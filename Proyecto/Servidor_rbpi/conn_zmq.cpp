/***********************************************************************
 * Archivo: conn_zmq.cpp                                                   *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo     *
 * Matarrubia.                                                         *
 **********************************************************************/

#include "conn_zmq.h"

//Atributos estáticos clases de socket
context_t conn_zmq::context(1);

//Inicializa la escucha en una IP, puerto u protocolo definido
void conn_zmq::bind(std::string dirIP){
    s.bind(dirIP.c_str());
}

//Conecta el socket a una IP, puerto y protocolo definido
void conn_zmq::connect(std::string dirIP){
    s.connect(dirIP.c_str());
    connected = true;
}

//Recibe un Command que lo parsea y lo guarda.
Command conn_zmq::recv_command( ){ //TODO:hacer una escucha no bloqueante
    message_t in;
    //Esperamos respuesta
    s.recv (&in);
    std::string msg_str(static_cast<char*>(in.data()), in.size());
    msjCmd.ParseFromString(msg_str);
    std::string text_str;
    std::cout << "Recibido :" << msjCmd.cmd() << std::endl;
    return msjCmd;
}

//Envía un mensaje serializado de tipo Command
void conn_zmq::send_command( Command msjCmd ){
    std::string msg_str;
    msjCmd.SerializeToString(&msg_str);
    // Creamos un mensaje zmq con el Command serializado
    message_t out (msg_str.size());
    memcpy ((void *) out.data (), msg_str.c_str(), msg_str.size());
    std::cout << "Enviando respuesta: " << msjCmd.cmd() << std::endl;
    s.send (out);
}

//Envía un mensaje serializado de tipo State
void conn_zmq::send_state( State msjSt ){
    std::string msg_str;
    msjSt.SerializeToString(&msg_str);
    // Creamos un mensaje zmq con el State serializado
    message_t out (msg_str.size());
    memcpy ((void *) out.data (), msg_str.c_str(), msg_str.size());
    std::cout << "Enviando respuesta: " << msjSt.state() << std::endl;
    s.send (out);
}

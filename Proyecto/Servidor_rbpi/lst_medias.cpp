/***********************************************************************
 * Archivo: lst_medias.cpp                                             *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 **********************************************************************/
#include "lst_medias.h"

lst_medias::lst_medias(string _dir) {
    dir = _dir;
}

int lst_medias::cargarlista() {
    DIR *dp;
    struct dirent *entry;
    struct stat statbuf;
    int index = 0;
    if ((dp = opendir(dir.c_str())) == NULL) {
        cerr<< "cannot open directory: "<< dir << endl;
        return 0;
    }
    chdir(dir.c_str());
    while ((entry = readdir(dp)) != NULL) {
        lstat(entry->d_name, &statbuf);
        //Si el fichero es un directorio se lo salta
        if (S_ISDIR(statbuf.st_mode)) continue;
        else {
            //TODO:Comprobar que es un archivo .mp4
            nombre[index] = entry->d_name;
            path[index] = dir + nombre[index];
            cout << path[index] << endl;
            index++;
        }
    }
    chdir("..");
    closedir(dp);
    return index;
}
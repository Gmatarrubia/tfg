/***********************************************************************
 * Archivo: gestorMP.cpp                                               *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 **********************************************************************/

#include "gestorMP.h"

//La clase gestorMP sólo trabaja con punteros
gestorMP::gestorMP(vlc_object *_v, conn_zmq *_z) {
    v=_v;
    z=_z;
}

/* Segun se recibe un comando se actua en consecuencia y se envía
 * un State de vuelta. */
void gestorMP::gestion_recv_cmd() {
    //Recibir comando de la GUI
    z->msjCmd = z->recv_command ( );
    //Interpretar el comando
    switch( z->msjCmd.cmd() ){
        case 1:
            //Solamente reproducir el video cargado
            v->Play();
            break;
        case 2:
            //Alternar entre pausa y play
            v->ResumePause();
            break;
        case 3:
            //Parar video
            v->Stop();
            break;
        case 4:
            //Solamente cargar un video nuevo
            cargar_video();
            break;
        case 5:
            //Orden vacia, reservado para primera entrega de paths
            break;
        case 6:
            //Cambiar path y play
            cargar_video();
            v->Play();
            break;
        case 7:
            //Liberar instancia
            v->release_vlc_inst();
        default:
            break;
    }
    //Responder con el estado
    z->msjSt.set_state( v->get_state() );
    z->send_state( z->msjSt );
}

void gestorMP::preparar_envio_paths() {
    for(int i=0; i<v->get_index(); i++) z->msjSt.add_path(v->myvideos.nombre[i]);
}

void gestorMP::cargar_video() {
    int izq, arriba, dcha, abajo, audio, rot;
    izq = z->msjCmd.prop().izq();
    arriba = z->msjCmd.prop().arriba();
    dcha = z->msjCmd.prop().dcha();
    abajo = z->msjCmd.prop().abajo();
    rot = z->msjCmd.prop().rot();
    audio = z->msjCmd.prop().audio();
    v->new_engine( z->msjCmd.path(), izq, arriba, dcha, abajo, audio, rot);
}
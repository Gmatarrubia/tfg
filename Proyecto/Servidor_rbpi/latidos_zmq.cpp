/***********************************************************************
 * Archivo: latidos_zmq.cpp                                                   *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo     *
 * Matarrubia.                                                         *
 **********************************************************************/

#include "latidos_zmq.h"
#include <unistd.h>

//Conecta el socket y crea el thread
void latidos_zmq::send_latidos_thread() {
    connect(ip_broker);
    connected = true;
    pthread_create(&wk_thread, NULL, &thread, this);
}
//Funcion estática para poder crear un thread que apunte a un método
void* latidos_zmq::thread(void* arg) {
    return static_cast<latidos_zmq*>(arg)->send_latidos();
}

//Funcíon de ejecución continua que envía un latido cada 5 segundos.
void* latidos_zmq::send_latidos() {
    if (connected == false) return this;
    std::string msg_up;
    mylatido.set_ip(my_ip);
    mylatido.set_state(1);
    for(int i=0 ; i<8; i) { //bucle infinito
        int st = v->get_state();
        mylatido.set_state(st);
        send_state(mylatido);
        sleep(5);
    }
    return this;
}
void latidos_zmq::send_state( Latido msjSt){
    std::string msg_str;
    msjSt.SerializeToString(&msg_str);
    // Creamos un mensaje zmq con el State serializado
    message_t out (msg_str.size());
    memcpy ((void *) out.data (), msg_str.c_str(), msg_str.size());
    std::cout << "Enviando Latido: " << msjSt.state() << std::endl;
    s.send (out);
}

latidos_zmq::~latidos_zmq() {
    mylatido.set_state(0);
    send_state(mylatido);
    pthread_exit(&wk_thread);
}
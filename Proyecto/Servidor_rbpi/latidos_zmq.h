/***********************************************************************
 * Archivo: latidos_zmq.h                                              *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 **********************************************************************/

#include "conn_zmq.h"
#include <pthread.h>
#include <iostream>
#include "vlc_object.h"

#ifndef SERVIDOR_RBPI_LATIDOS_ZMQ_H
#define SERVIDOR_RBPI_LATIDOS_ZMQ_H

/*Latidos_zmq es un tipo especial de connexion zmq ya que su funcion
 * es muy específica. Crear un thread en el que se envian latidos cada
 * 5 segundos*/
class latidos_zmq : public conn_zmq {
protected:
    pthread_t wk_thread;
    std::string ip_broker;
    std::string my_ip;
    vlc_object *v;
    static void *thread(void* arg);
    Latido mylatido;
public:
    latidos_zmq(std::string _ip_broker, std::string _my_ip, vlc_object *_myVlc) : conn_zmq(ZMQ_DEALER){
        v = _myVlc;
        ip_broker = _ip_broker;
        my_ip = _my_ip;
    }
    ~latidos_zmq();
    void send_latidos_thread();
    void* send_latidos();
    void send_state(Latido msjSt);
};

#endif //SERVIDOR_RBPI_LATIDOS_ZMQ_H
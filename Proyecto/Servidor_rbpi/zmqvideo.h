/***********************************************************************
 * Archivo: zmqvideo.h                                                 *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 **********************************************************************/

#include <iostream>
#include "conn_zmq.h"
#include <pthread.h>

#ifndef SERVIDOR_RBPI_MYZMQVIDEO_H
#define SERVIDOR_RBPI_MYZMQVIDEO_H

/* myzmqvideo es un tipo especial de connexion zmq ya que su funcion
 * es muy específica. Crear un thread para habilitar la descarga de
 * videos */
class zmqVideo :  public conn_zmq {
protected:
    pthread_t wk_thread;
    static void *thread(void* arg);
public:
    zmqVideo() : conn_zmq(ZMQ_PAIR) {
    }
    void recv_videos_thread();
    void* recv_videos();
    void recv_data();

};


#endif //SERVIDOR_RBPI_MYZMQVIDEO_H

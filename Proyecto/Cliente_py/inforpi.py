#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
* Archivo: inforpi.py                                                 *
* Este es un documento que forma parte del Trabajo Fin de Grado de    *
* Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
* Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
from Tkinter import *

class Inforpi(Frame):
    RPI = "rpi.png"
    PORTATIL = "portatil.png"

    def __init__(self, parent, ventana, gestor):
        self.ventana = ventana
        pageInfo = ventana.pageInfo
        self.gestor = gestor
        Frame.__init__(self, pageInfo)
        #Frame de titulo
        self.ftitulo = ventana.crearFrame(self, 0, 0)
        self.ftitulo.config(width=ventana.anchura-5, height=ventana.altura/10.)
        self.ftitulo.grid_columnconfigure(0, weight=1)
        self.ftitulo.grid_rowconfigure(0, weight=1)
        self.ftitulo.grid_propagate(False)
        self.ftitulo1 = ventana.crearFrame(self.ftitulo, 0, 0, 1, 1, FLAT)
        self.titulo = ventana.crearEtiqueta(self.ftitulo1, "Estado Equipos conectados", 0, 0, 1, 2)
        self.titulo.config(font=("Courier", 22))
        self.txt1 = ventana.crearEtiqueta(self.ftitulo1, "Aplicar alias:", 1, 0)
        self.b1 = ventana.crearBoton(self.ftitulo1, "Aplicar", self.aplicarAlias, ACTIVE, 1, 1)
        #Frame de dibujo de estados equipos.
        self.dicMarcoImgs = {}
        self.finfo = ventana.crearFrame(self, 1, 0)
        myPC = ventana.crearMarcoImg(self.finfo, "portatil.png", 0, 0, "My PC")
        self.dicMarcoImgs['portatil'] = myPC
        self.finfo.grid_columnconfigure(1, weight=1)
        self.separador = ventana.crearSeparador(self.finfo, "horizontal", 0, 1)


    def addEquipo(self, dirIP, num):
        self.dicMarcoImgs[dirIP] = (self.ventana.crearMarcoImg(self.finfo, self.RPI,
                                                               num/5, (num%5)+1, dirIP))

    def chgEstado(self, dirIP, estado):
        self.dicMarcoImgs[dirIP][4].config(text=estado)

    def chgNombre(self, dirIP, nombre):
        self.dicMarcoImgs[dirIP][2].config(text=nombre)

    def destroyInfoServer(self, server):
        self.dicMarcoImgs[server][2].config(text='')
        self.dicMarcoImgs[server][4].config(text='')
        self.dicMarcoImgs[server][3].destroy()
        self.dicMarcoImgs.pop(server)

    def aplicarAlias(self):
        for server in self.dicMarcoImgs:
            alias = self.dicMarcoImgs[server][3].get()
            if len(alias) < 2:
                continue
            if self.gestor.dicServidores.has_key(server):
                self.gestor.dicServidores[alias] = self.gestor.dicServidores[server]
                self.gestor.dicServidores.pop(server)
                self.dicMarcoImgs[alias] = self.dicMarcoImgs[server]
                self.dicMarcoImgs.pop(server)
                self.gestor.cambiarAlias(server, alias)
                self.dicMarcoImgs[alias][2].config(text=alias)
                self.dicMarcoImgs[alias][3].delete(0, END)
            else:
                self.dicMarcoImgs[server][2].config(text=alias)
                self.dicMarcoImgs[server][3].delete(0, END)

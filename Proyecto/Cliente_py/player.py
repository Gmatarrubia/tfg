#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: player.py                                                  *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""

class Player(object):
    CMDTYPES = {"Play": 1, "Pause": 2, "Stop": 3, "Carga file": 4}
    STTYPES = {"Ocioso": 0, "Abierto": 1, "Playing": 3,
               "Pausado": 4, "Parado": 5, "Terminado": 6, "Error": 7}
    AUDIO = {"Con audio": 1, "Silencioso": 0}

    def __init__(self, _id, video=None, vw=None, duration=0, mode=1):
        self.alias = ""
        self.videos = {}
        self.vw = vw
        self.mode = mode
        self.estado = 0
        self.duracion = duration
        self.id = _id
    def chgmode(self, m):
        self.mode = m
    def set_audio(self, a):
        self.vw.establecer_audio(a)
    def set_path(self, p):
        self.vw.establecer_path(p)
    def start(self):
        self.vw.establecer_cmd(self.CMDTYPES["Play"])
        self.vw.envio_cmd()
    def solicitar_paths(self):
        return self.vw.solicitar_paths()
    def obtener_estado(self):
        return self.vw.estado.state

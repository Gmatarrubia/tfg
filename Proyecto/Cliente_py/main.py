#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: main.py                                                    *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
import zmq
from gui import gui
from gestorzmq import Gestorzmq
from gestorvw import Gestorvw
from myzmq import myzmq
from mylatido import mylatido
from watchdog import Watchdog
from gestorVideos import GestorVideos

discorverSocket = myzmq(zmq.ROUTER)
discorverSocket.initServer("tcp://192.168.1.40:2322")
ventana = gui()
gestor = Gestorzmq(ventana)
gestorvw = Gestorvw(ventana, gestor)
gestorVids = GestorVideos(gestor)
gestorVids.start()
recibeLatidos = mylatido(discorverSocket, gestor)
recibeLatidos.start()
vigilante = Watchdog(gestor)
vigilante.start()

ventana.loop()

#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: gestorVideos.py                                            *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
########PARA USAR ESTE MODULO ANTES USAR: ###########
#import imageio
#imageio.plugins.ffmpeg.download()
#####################################################
import time
from threading import Thread
from os import listdir
from os.path import isfile, join

from moviepy.editor import VideoFileClip

class VideoClip(VideoFileClip):
    def __init__(self, filename, video):
        self.timeStart = 0
        self.name = video
        VideoFileClip.__init__(self, filename)

class GestorVideos(Thread):
    extVideos = ['ASF', 'AVI', 'BIK', 'DIV', 'DIVX', 'DVD', 'IVF', 'M1V',
                 'MOV', 'MOVIE', 'MP2V', 'MP4', 'MPA', 'MPE', 'MPEG', 'MPG',
                 'MPV2', 'QT', 'QTL', 'RPM', 'SMK', 'WM', 'WMV', 'WOB']

    def __init__(self, gestor):
        self.videos = {}
        self.exts = {}
        Thread.__init__(self, target=self.refresco, args=(gestor, 0))

    def refresco(self, gestor, nada):
        while True:
            mi_path = "./videos/"
            video = ""
            numero = len(self.videos)
            for f in listdir(mi_path):
                fichero = join(mi_path, f)
                if isfile(fichero):
                    ext = fichero.split('.')[2].upper()
                    if ext in self.extVideos:
                        video = fichero.split('/')[2].split('.')[0]
                        if not self.videos.has_key(video):
                            vid = VideoClip(fichero, video)
                            self.videos[video] = vid
                            self.exts[video] = ext
            if len(self.videos) - numero >= 1:
                gestor.addVideos(self.videos, self.exts)
            time.sleep(5)

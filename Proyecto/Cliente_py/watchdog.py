#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: watchdog.py                                                *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""

import time
from threading import Thread

class Watchdog(Thread):
    def __init__(self, gestor):
        Thread.__init__(self, target=self.vigilancia, args=(gestor, 0))
    def vigilancia(self, gestor, nada):
        while True:
            now = time.time()
            gestor.Watchdog(now)
            time.sleep(5)

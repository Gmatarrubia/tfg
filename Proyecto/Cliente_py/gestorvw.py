#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: gestorvw.py                                                *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
import math
from copy import copy
from Tkinter import *
from videowall import VideoWall
from servidor import servidor
from mensaje_pb2 import State
from infovw import Infovw

class Gestorvw(Frame):
    mousefromX = 0
    mousefromY = 0
    angleStart = (0, 0)
    centro = (0, 0)
    def __init__(self, ventana, gestor):
        self.dicServidores = gestor.dicServidores
        self.gestor = gestor
        pageVW = ventana.pageVideoWalls
        Frame.__init__(self, pageVW)
        self.seleccion = []
        self.vwseleccion = VideoWall()
        self.refers = []
        self.FormatoMedidas = IntVar(self, 1)
        self.MedidasVWcm = [47.6, 26.79]
        self.MedidaX = StringVar()
        self.MedidaX.set(str(self.MedidasVWcm[0]))
        self.MedidaY = StringVar()
        self.MedidaY.set(str(self.MedidasVWcm[1]))
        msj = State()
        msj.IP = '0'
        self.svrseleccion = servidor(msj, 3)
        self.titulo = ventana.crearEtiqueta(self, "Gestor de video walls", 0, 0, 1, 6)
        self.titulo.config(font=("Courier", 22))
        self.nombre = ventana.crearEtiqueta(self, "Nombre del video wall:", 1, 0)
        self.entryname = ventana.crearEntrada(self, 1, 1)
        self.entryname.config(width=20)
        #Configuracion y creacion del canvas
        self.screen = ventana.crearCanvas(self, 2, 0, 6, 6)
        self.canvasH = ventana.altura*0.65
        self.canvasW = self.canvasH*1.77 #ventana.anchura*0.65
        self.screen.config(width=self.canvasW, height=self.canvasH, bg='gray78')
        self.screen.create_line([15, 15, 70, 15], width=4, arrow='last', fill='olive drab')
        self.screen.create_line([15, 15, 15, 70], width=4, arrow='last', fill='DeepSkyBlue4')
        self.screen.create_text([80, 15], text='X')
        self.screen.create_text([15, 80], text='Y')
        #Widgets para configurar las medidas reales que respresenta el video en cm
        self.fMedidas = ventana.crearFrame(self, 8, 0, 1, 6)
        self.texto1 = ventana.crearEtiqueta(self.fMedidas,
                                            "Introducir medidas reales del video:", 0, 0)
        self.RBvertical = ventana.crearRadioBot(self.fMedidas,
                                                "Vertical", self.FormatoMedidas, 0, 0, 1)
        self.RBHorizontal = ventana.crearRadioBot(self.fMedidas,
                                                  "Horizontal", self.FormatoMedidas, 1, 0, 2)
        self.entryMedidas = ventana.crearEntrada(self.fMedidas, 0, 3)
        self.entryMedidas.bind("<FocusIn>", self.limpiarEntryMedidas)
        self.entryMedidas.insert(END, 'cm')
        self.entryMedidas.config(justify=CENTER, width=10)
        self.aplicarMedidas = ventana.crearBoton(self.fMedidas, "Aplicar",
                                                 self.actualizarMedidas, ACTIVE, 0, 4)
        self.sep2 = ventana.crearSeparador(self.fMedidas, "vertical", 0, 5)
        self.texto1 = ventana.crearEtiqueta(self.fMedidas, "Medidas actuales:", 0, 6)
        self.texto2 = ventana.crearEtiquetaVar(self.fMedidas, self.MedidaX, 0, 7)
        self.texto2.config(justify=CENTER, font=("Times", 10, "bold"))
        self.texto3 = ventana.crearEtiqueta(self.fMedidas, " x ", 0, 8)
        self.texto4 = ventana.crearEtiquetaVar(self.fMedidas, self.MedidaY, 0, 9)
        self.texto4.config(justify=CENTER, font=("Times", 10, "bold"))
        #Bindings y eventos dentro del canvas
        self.screen.bind("<ButtonPress-1>", self.click)
        self.screen.bind("<B1-Motion>", self.arrastrar)
        self.screen.bind("<ButtonRelease-1>", self.marcarCoords)
        self.screen.bind("<ButtonPress-3>", self.getStartAngle)
        self.screen.bind("<B3-Motion>", self.rotar)
        self.screen.bind("<ButtonRelease-3>", self.marcarAngle)
        self.screen.bind("<Enter>", self.activarScroll)
        self.screen.bind("<Leave>", self.desactivarScroll)
        #Botones laterales
        self.l1 = ventana.crearLista(self, [0], 3, 6, self.actualizarLista)
        self.l1.bind("<<ComboboxSelected>>", self.seleccionServer)
        self.Raspecto = StringVar()
        self.Raspecto.set("16:9")
        self.f2 = ventana.crearLabelFrame(self, "R. Aspecto", 4, 6)
        self.formato169 = ventana.crearRadioBot(self.f2, "16:9", self.Raspecto, "16:9", 0, 0)
        self.formato43 = ventana.crearRadioBot(self.f2, "4:3", self.Raspecto, "4:3", 1, 0)
        self.formato11 = ventana.crearRadioBot(self.f2, "1:1", self.Raspecto, "1:1", 2, 0)
        self.newRect = ventana.crearBoton(self, "Intr Rpi", self.crearRectangulo, ACTIVE, 5, 6)
        self.delRect = ventana.crearBoton(self, "Supr Rpi", self.eliminarRectangulo, ACTIVE, 6, 6)
        self.gen = ventana.crearBoton(self, "Crear VideoWall", self.crearVideowall, ACTIVE, 1, 2)
        self.delet = ventana.crearBoton(self, "Supr VideoWall", self.suprVideowall, ACTIVE, 1, 5)
        self.sep1 = ventana.crearSeparador(self, "vertical", 1, 3)
        self.lstvw = ventana.crearLista(self, [0], 1, 4, self.actualizarLista)
        self.lstvw.bind("<<ComboboxSelected>>", self.seleccionVW)
        self.infoVW = Infovw(self, ventana, gestor)
        self.infoVW.grid(row=0, column=1, sticky="ns")
        self.grid(row=0, column=0)

    def crearVideowall(self):
        nombre = self.entryname.get()
        self.dicServidores[nombre] = copy(self.vwseleccion)
        self.gestor.studio.selServidor = self.dicServidores[nombre]
        self.gestor.studio.selAlias = nombre
        self.gestor.studio.configNewPlayer()
        self.screen.delete('servidor')
        #Destruir servidores incluidos en el vw del dic general

    def suprVideowall(self):
        nombre = self.lstvw.get()
        for server in self.dicServidores[nombre].dicServidores:
            s = self.dicServidores[nombre].dicServidores[server]
            s.enVW = 0
            s.posIzq = 0
            s.posArriba = 0
            s.posDcha = 0
            s.posAbajo = 0
        self.dicServidores.pop(nombre)
        self.actualizarLista()
        self.vwseleccion = VideoWall()
        self.screen.delete('servidor')
        self.lstvw.set('0')
        #Devolver los servidores del vw al dic general

    def crearRectangulo(self):
        if self.svrseleccion:
            raspecto = self.Raspecto.get()
            if raspecto == "16:9":
                self.screen.create_polygon([(50, 25), (250, 25), (250, 137.5), (50, 137.5)],
                                           fill="blue", state='normal', tags=['servidor', 'rec', '16:9'])
                coordText = (150, 81.25)
            elif raspecto == "4:3":
                self.screen.create_polygon([(50, 25), (250, 25), (250, 175), (50, 175)],
                                           fill="blue", state='normal', tags=['servidor', 'rec', '4:3'])
                coordText = (150, 100)
            elif raspecto == "1:1":
                self.screen.create_polygon([(50, 25), (250, 25), (250, 225), (50, 225)],
                                           fill="blue", state='normal', tags=['servidor', 'rec', '1:1'])
                coordText = (150, 125)
            ip = self.l1.get()
            self.screen.create_text(coordText, text=ip, state='disabled', tags=['servidor'])
            self.screen.addtag_overlapping(ip, coordText[0]-10, coordText[1]-10,
                                           coordText[0]+10, coordText[1]+10)
            self.vwseleccion.dicServidores[ip] = self.dicServidores[ip]
            self.dicServidores[ip].enVW = 1
            self.l1.set('0')

    def eliminarRectangulo(self):
        if self.seleccion:
            tags = self.screen.gettags(self.seleccion[0])
            self.vwseleccion.dicServidores.pop(tags[3])
            self.screen.delete(tags[3])
            self.dicServidores[tags[3]].enVW = 0

    def click(self, event):
        x = self.mousefromX = event.x
        y = self.mousefromY = event.y
        for item in self.seleccion:
            if self.screen.itemcget(item, 'state') == 'normal':
                self.screen.itemconfig(item, fill="blue")
        self.seleccion = self.screen.find_overlapping(x-10, y-10, x+10, y+10)
        for item in self.seleccion:
            if self.screen.itemcget(item, 'state') == 'normal':
                self.screen.itemconfig(item, fill="red")
                tags = self.screen.gettags(item)
                self.svrseleccion = self.vwseleccion.dicServidores[tags[3]]
                self.localizarRefers()
                self.infoVW.actualizarCoords(self.refers)

    def arrastrar(self, event):
        #Arrastrar la seleccion según el mouse
        for item in self.seleccion:
            if self.screen.itemcget(item, 'state') == 'normal':
                tags = self.screen.gettags(item)
                self.screen.move(tags[3],
                                 (-self.mousefromX+event.x)/20,
                                 (-self.mousefromY+event.y)/20)
                self.localizarRefers()
                self.infoVW.actualizarCoords(self.refers)

    def fijarposicion(self, coords):
        #Fijar un rectangulo en una posicion determinada
        cAntiguo = copy(self.localizarCentro())
        cNuevo = 0
        for item in self.seleccion:
            if self.screen.itemcget(item, 'state') == 'normal':
                tags = self.screen.gettags(item)
                self.screen.coords(tags[3], *coords)
                self.localizarRefers()
                self.infoVW.actualizarCoords(self.refers)
                cNuevo = self.localizarCentro()
                self.seleccion = self.screen.find_withtag(tags[0])
        for item in self.seleccion:
            if self.screen.itemcget(item, 'state') == 'disabled':
                text = self.screen.find_closest(cAntiguo[0], cAntiguo[1])
                self.screen.move(text, cNuevo[0]-cAntiguo[0], cNuevo[1]-cAntiguo[1])

    def marcarCoords(self, event=0):
        for item in self.seleccion:
            if self.screen.itemcget(item, 'state') == 'normal':
                width = self.MedidasVWcm[0]
                height = self.MedidasVWcm[1]
                xy = self.screen.coords(item)
                x1cm = (xy[0]*width)/self.canvasW
                y1cm = (xy[1]*height)/self.canvasH
                x2cm = (xy[2]*width)/self.canvasW
                y2cm = (xy[7]*height)/self.canvasH
                self.svrseleccion.posIzq = int(round((x1cm/width)*100))
                self.svrseleccion.posArriba = int(round((y1cm/height)*100))
                self.svrseleccion.posDcha = int(round((x2cm/width)*100))
                self.svrseleccion.posAbajo = int(round((y2cm/height)*100))

    def getAngle(self, event):
        if self.seleccion:
            self.localizarCentro()
            dx = self.screen.canvasx(event.x) - self.centro[0]
            dy = self.screen.canvasy(event.y) - self.centro[1]
            try:
                return complex(dx, dy) / abs(complex(dx, dy))
            except ZeroDivisionError:
                return 0.0 # cannot determine angle

    def getStartAngle(self, event):
        self.angleStart = self.getAngle(event)

    def rotar(self, event):
        if self.seleccion:
            angle = self.getAngle(event) / self.angleStart
            offset = complex(self.centro[0], self.centro[1])
            newxy = []
            for item in self.seleccion:
                if self.screen.itemcget(item, 'state') == 'normal':
                    lstxy = self.screen.coords(item)
                    i = iter(lstxy)
                    xy = zip(i, i)
                    for x, y in xy:
                        v = angle * (complex(x, y) - offset) + offset
                        newxy.append(v.real)
                        newxy.append(v.imag)
                    self.screen.coords(item, *newxy)
                    #Calculo de giro para mostrar y enviar
                    c = self.screen.coords(item)
                    horizontal = c[4] - c[6]
                    vertical = c[7] - c[5]
                    print horizontal, vertical
                    grados = math.atan(vertical/horizontal)
                    grados = (grados*360)/(2*math.pi)
                    if vertical < 0:
                        if horizontal < 0:
                            grados += 180 #cuarto cuadrante
                        else:
                            grados += 360 #tercer cuadrante
                    else:
                        if horizontal < 0:
                            grados += 180 #segundo cuadrante
                        else:
                            grados += 0 #primer cuadrante
                    self.infoVW.actualizarRot(int(round(grados)))
                    grados = 360 - grados #El video debe compensar giro de pantalla
                    self.svrseleccion.rot = int(round(grados))

    def marcarAngle(self, event):
        """
        for item in self.seleccion:
            if self.screen.itemcget(item, 'state') == 'normal':
                complejo = self.getAngle(event)
                grados = math.atan(complejo.imag/complejo.real)
                grados = (grados*360)/(2*math.pi)
                self.svrseleccion.rot = int(round(grados))
                self.infoVW.actualizarRot(int(round(grados)))
        """
        pass


    def activarScroll(self, event):
        #Para Windows
        self.screen.bind_all("<MouseWheel>", self.scroll)
        # with Linux OS
        self.screen.bind_all("<Button-4>", self.scroll)
        self.screen.bind_all("<Button-5>", self.scroll)

    def desactivarScroll(self, event):
                #Para Windows
        self.screen.unbind_all("<MouseWheel>")
        # with Linux OS
        self.screen.unbind_all("<Button-4>")
        self.screen.unbind_all("<Button-5>")

    def scroll(self, event):
        self.localizarCentro()
        for item in self.seleccion:
            tags = self.screen.gettags(item)
            if event.num == 5 or event.delta == -120:
                self.screen.scale(tags[3], self.centro[0], self.centro[1], 0.9, 0.9)
            if event.num == 4 or event.delta == 120:
                self.screen.scale(tags[3], self.centro[0], self.centro[1], 1.1, 1.1)
            self.marcarCoords()
            self.localizarRefers()
            self.infoVW.actualizarCoords(self.refers)

    def localizarCentro(self):
        for item in self.seleccion:
            if self.screen.itemcget(item, 'state') == 'normal':
                coords = self.screen.bbox(item)
                self.centro = [(coords[0]+coords[2])/2., (coords[1]+coords[3])/2.]
                return self.centro

    def localizarRefers(self):
        for item in self.seleccion:
            if self.screen.itemcget(item, 'state') == 'normal':
                width = self.MedidasVWcm[0]
                height = self.MedidasVWcm[1]
                xy = self.screen.coords(item)
                x1cm = round((xy[0]*width)/self.canvasW, 2)
                y1cm = round((xy[1]*height)/self.canvasH, 2)
                x2cm = round((xy[2]*width)/self.canvasW, 2)
                y2cm = round((xy[7]*height)/self.canvasH, 2)
                self.refers = [x1cm, x2cm, y1cm, y2cm]

    def seleccionVW(self, event=0):
        vw = self.lstvw.get()
        self.vwseleccion = vw

    def seleccionServer(self, event=0):
        self.svrseleccion = self.l1.get()

    def actualizarLista(self):
        lstVideowalls = []
        lstServersFree = []
        #Actualizamos la lista de VideoWalls
        for server in self.dicServidores.keys():
            if isinstance(self.dicServidores[server], VideoWall):
                lstVideowalls.append(server)
        self.lstvw['values'] = lstVideowalls
        #Actualizamos la lista de Servers libres (no utilizados)
        for server in self.dicServidores.keys():
            if self.dicServidores[server].enVW == 0:
                lstServersFree.append(server)
        self.l1['values'] = lstServersFree

    def limpiarEntryMedidas(self, event=0):
        self.entryMedidas.delete(0, END)

    def actualizarMedidas(self):
        horientacion = self.FormatoMedidas.get()
        medida = float(self.entryMedidas.get())
        if horientacion:
            self.MedidasVWcm[0] = medida
            self.MedidaX.set(str(medida))
            medida2 = round(medida/1.777, 2)
            self.MedidasVWcm[1] = medida2
            self.MedidaY.set(medida2)
        else:
            self.MedidasVWcm[1] = medida
            self.MedidaY.set(str(medida))
            medida2 = round(medida*1.777, 2)
            self.MedidasVWcm[0] = medida2
            self.MedidaX.set(medida2)

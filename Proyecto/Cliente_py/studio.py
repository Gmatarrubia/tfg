#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: estudio.py                                                 *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
from Tkinter import *
from copy import copy
from player import Player
from panelplayer import PanelPlayer
from servidor import servidor
from mensaje_pb2 import State
import zmq

class Studio(Frame):
    CMDTYPES = {"Play": 1, "Pause": 2, "Stop": 3, "Carga file": 4}
    STTYPES = {"Ocioso": 0, "Abierto": 1, "Playing": 3,
               "Pausado": 4, "Parado": 5, "Terminado": 6, "Error": 7}
    AUDIO = {"Con audio": 1, "Silencioso": 0}

    def __init__(self, parent, _ventana, gestorZMQ):
        self.dicServidores = gestorZMQ.dicServidores
        self.alias = gestorZMQ.alias
        self.ventana = _ventana
        self.selServidor = servidor(State(), zmq.REQ)
        self.selAlias = None
        self.selPanel = None
        Frame.__init__(self, parent)
        self.players = {}
        self.iconos = {}
        self.nombres = {}
        self.ids = {}
        self.paneles = {}
        self.videos = {}
        self.exts = {}
        self.iconos2 = {}
        self.nombres2 = {}
        self.num = 0
        self.f1 = self.ventana.crearFrame(self, 0, 0, 2, 1)
        self.f2 = self.ventana.crearFrame(self, 0, 1)
        self.f3 = self.ventana.crearFrame(self, 1, 1)
        self.f1.config(width=(self.ventana.anchura*2/3.)-5, height=self.ventana.altura-100)
        self.f1.columnconfigure(0, weight=1)
        self.f1.grid_propagate(False)
        self.f2.config(width=(self.ventana.anchura*1/3.)-5, height=(self.ventana.altura-100)/2.)
        self.f2.columnconfigure(0, weight=1)
        self.f2.grid_propagate(False)
        self.f3.config(width=(self.ventana.anchura*1/3.)-5, height=(self.ventana.altura-100)/2.)
        self.f3.columnconfigure(0, weight=1)
        self.f3.grid_propagate(False)
        #Dentro del frame 1
        t1 = "Gestor de Reproductores"
        self.titulo1 = self.ventana.crearEtiqueta(self.f1, t1, 0, 0)
        self.titulo1.config(font=("Courier", 22))
        self.bplayall = self.ventana.crearBoton(self.f1, "Reproducir Todo", self.playall, ACTIVE, 1, 0)
        np = int(self.f1.cget('height'))/150
        for n in range(np):
            self.paneles[n] = PanelPlayer(self.f1, self.ventana)
            self.paneles[n].grid(row=2+n, column=0, columnspan=4)
            self.paneles[n].bindtags(("panel",) + self.paneles[n].bindtags())
        self.ventana.ventana.bind_class("panel", "<ButtonPress-1>", self.seleccionPlayer)
        #self.paneles[n].bind("<Enter>", self.seleccionPlayer)
        #Recursos gráficos frame 2
        w = self.f2.cget('width')
        h = self.f2.cget('height')
        self.wImg = wImg = w/9
        self.imgSizeMinis = imgSizeMinis = [wImg, wImg]
        self.imgVacio = self.ventana.crearImagenRAW("vacio.png", imgSizeMinis)
        self.imgLleno = self.ventana.crearImagenRAW("lleno.png", imgSizeMinis)

        #Dentro del frame 2
        t2 = "Lista de Reproductores"
        self.titulo2 = self.ventana.crearEtiqueta(self.f2, t2, 0, 0)
        self.titulo2.config(font=("Courier", 22))
        self.minis = self.ventana.crearCanvas(self.f2, 1, 0)
        self.minis.config(width=w-6, height=h-20)
        for n in range(24):
            coord = [0, 0]
            coord[0] = (n%6)*(wImg*1.5)+(wImg/1.5)
            coord[1] = (n/6)*(wImg*1.5)+(wImg/1.5)
            coordID = [coord[0]-wImg/4, coord[1]-wImg/4]
            self.players[n] = Player(n)
            self.iconos[n] = self.minis.create_image(coord, image=self.imgVacio,
                                                     tags=['player', n, 'img'])
            self.ids[n] = self.minis.create_text(coordID, text=n,
                                                 font=("Courier", 8),
                                                 tags=['player', n, 'txt', 'id'])
            self.nombres[n] = self.minis.create_text(coord, text="Disponible",
                                                     font=("Courier", 6),
                                                     tags=['player', n, 'txt', 'nombre'])
        #Recursos graficos frame 3
        #self.imgVideo = self.ventana.crearImagenRAW("video.png", imgSizeMinis)
        self.imgVideo = {}
        #Dentro del frame 3
        t3 = "Lista de videos"
        self.titulo3 = self.ventana.crearEtiqueta(self.f3, t3, 0, 0)
        self.titulo3.config(font=("Courier", 22))
        self.cvids = self.ventana.crearCanvas(self.f3, 1, 0)
        self.cvids.config(width=w-6, height=h-20)

        #Bindings
        self.minis.bind("<ButtonPress-1>", self.seleccion)
        self.minis.bind("<ButtonPress-3>", self.click2dario)
        self.cvids.bind("<ButtonPress-1>", self.seleccionVideo)

    def seleccionPlayer(self, event):
        for p in self.paneles:
            self.paneles[p].config(relief=FLAT, borderwidth=0)
            if self.paneles[p] == event.widget:
                event.widget.config(relief=GROOVE, borderwidth=2)
                self.selPanel = event.widget
                break
            else:
                for children in self.paneles[p].children.values():
                    if children == event.widget:
                        self.paneles[p].config(relief=GROOVE, borderwidth=2)
                        self.selPanel = self.paneles[p]
                        break

    def seleccion(self, event):
        n = int(self.getId(event, self.minis))
        for p in self.paneles:
            if self.paneles[p].player is None:
                self.paneles[p].setAll(copy(self.players[n]))
                break
        return n

    def playall(self):
        for p in self.paneles:
            if not self.paneles[p].player is None:
                self.paneles[p].botonPlay()

    def configNewPlayer(self):
        for n in self.iconos:
            tags = self.minis.gettags(self.iconos[n])
            new = 0
            for t in tags:
                if t == 'used':
                    new = 1
            if new == 0:
                self.players[n].vw = copy(self.selServidor)
                self.players[n].alias = self.selAlias
                self.minis.addtag_withtag('used', self.iconos[n])
                self.minis.itemconfig(self.iconos[n], image=self.imgLleno)
                self.minis.itemconfig(self.nombres[n], text=self.selAlias)
                #self.players[n].vw.establecer_cmd(4) #Para debug
                #self.players[n].vw.envio_cmd()
                break

    def destroyServer(self, server):
        for n in self.nombres:
            tags = self.minis.itemcget(self.nombres[n], 'text')
            if server in tags:
                self.num -= 1
                self.minis.dtag(self.iconos[n], 'used')
                self.players[n].vw = None
                self.players[n].path = None
                self.minis.itemconfig(self.iconos[n], image=self.imgVacio)
                self.minis.itemconfig(self.nombres[n], text="Disponible")
                break 
        for n in self.paneles:
            if self.paneles[n].nombreServidor == server:
                self.paneles[n].borrarAll()
                self.paneles[n].restablecer()
                break


    def click2dario(self, event):
        n = int(self.getId(event, self.minis))
        tags = self.minis.gettags(self.iconos[n])
        for t in tags:
            if t == 'used':
                self.num -= 1
                self.minis.dtag(self.iconos[n], 'used')
                self.players[n].vw = None
                self.players[n].path = None
                self.minis.itemconfig(self.iconos[n], image=self.imgVacio)
                self.minis.itemconfig(self.nombres[n], text="Disponible")

    def getId(self, event, parent):
        c = parent
        sel = c.find_overlapping(event.x-2, event.y-2, event.x+2, event.y+2)
        n = c.gettags(sel[0])[1]
        return n

    def chgAlias(self, server, alias):
        for n in self.nombres:
            txt = self.minis.itemcget(self.nombres[n], 'text')
            if txt == server:
                self.minis.itemconfig(self.nombres[n], text=alias)
                break
        for p in self.paneles:
            if self.paneles[p].nombreServidor == server:
                self.paneles[p].setNombreServidor(alias)
                self.paneles[p].actualizarTitulo()

    def chgVideos(self, videos, exts):
        self.videos = videos
        self.exts = exts
        wImg = self.wImg
        coord = [0, 0]
        n = 0
        for v in self.videos:
            img = 'vids/' + v + '.png'
            dir_img = './img/' + img
            self.videos[v].save_frame(dir_img, 2)
            self.imgVideo[v] = self.ventana.crearImagenRAW(img, self.imgSizeMinis)
            coord[0] = (n%6)*(wImg*1.5)+(wImg/1.5)
            coord[1] = (n/6)*(wImg*1.5)+(wImg/1.5)
            coordTxt = [coord[0], coord[1]+wImg/1.5]
            self.iconos2[n] = self.cvids.create_image(coord, image=self.imgVideo[v],
                                                      tags=['video', n, 'img'])
            self.nombres2[n] = self.cvids.create_text(coordTxt, text=v,
                                                      font=("Courier", 8),
                                                      tags=['video', n, 'txt', 'nombre'])
            n += 1

    def seleccionVideo(self, event):
        n = int(self.getId(event, self.cvids))
        txt = self.cvids.itemcget(self.nombres2[n], 'text')
        if not self.selPanel is None:
            self.selPanel.addvideo(txt, self.videos[txt], self.exts[txt])

    def actualizarLista(self):
        pass
        ###self.lstServer = self.dicServidores.keys()

    def selectorServidor(self, _selected):
        return self.dicServidores[_selected]

    def chgEstado(self, server, time):
        #self.players[server] TODO:Implementar
        pass

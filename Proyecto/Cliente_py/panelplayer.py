#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: panelplayer.py                                             *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
from Tkinter import *
import random
from copy import copy
from threading import Thread
import threading
import time
from myzmqVideo import myzmqVideo
import zmq
from mensaje_pb2 import Video

class PanelPlayer(Frame):
    COLORS = ['gold', 'blue', 'orange', 'khaki1', 'tan1', 'pink3',\
    'gray81', 'bisque2', 'cyan', 'lime green', 'salmon', 'PaleGreen1',\
    'SpringGreen2', 'sky blue', 'green yellow', 'chocolate2']
    def __init__(self, parent, _ventana):
        self.player = None
        self.videos = {}
        self.exts = {}
        self.iconVids = {}
        self.ventana = _ventana
        self.time = IntVar()
        self.scale = 1
        self.id = 1
        self.titulo = StringVar()
        self.nombreServidor = ""
        self.duracion = 3600
        self.x = 0
        self.y = 0
        self.selTags = []
        w = parent.cget('width')
        #Recursos graficos
        self.imgPlay = self.ventana.crearImagenRAW("playPause.gif", [30, 30])
        self.imgMin = self.ventana.crearImagenRAW("minimize.gif", [30, 30])
        self.imgRep = self.ventana.crearImagenRAW("repeat.gif", [30, 30])
        self.imgEvnt = self.ventana.crearImagenRAW("events.gif", [30, 30])
        Frame.__init__(self, parent, padx=10, pady=20)
        self.b1 = Button(self, image=self.imgPlay, command=self.botonPlay, state=ACTIVE)
        self.b1.grid(row=0, column=0)
        self.b2 = Button(self, image=self.imgMin, command=self.minimize, state=ACTIVE)
        self.b2.grid(row=1, column=0)
        self.b3 = Button(self, image=self.imgRep, command=self.chgmode, state=ACTIVE)
        self.b3.grid(row=0, column=1)
        self.b4 = Button(self, image=self.imgEvnt, command=self.menuEvents, state=ACTIVE)
        self.b4.grid(row=1, column=1)
        self.tLine = self.ventana.crearCanvas(self, 0, 2, 2, 1)
        self.tLine.config(height=70, width=(w/5)*4, bg='gray78', scrollregion=(0, 0, 70, 3600))
        self.tLine.bindtags(("panel",) + self.tLine.bindtags())
        self.h = self.tLine.cget('height')
        self.rotulo = Label(self, textvariable=self.titulo, justify=CENTER)
        self.rotulo.grid(row=2, column=0, rowspan=1, columnspan=3, sticky='WE')
        self.rotulo.config(bg='gray88', bd=0)
        self.rotulo.bindtags(("panel",) + self.tLine.bindtags())
        #Dentro del canvas
        self.tLine.create_line([0, 69, 3600, 69], width=3, tags=['time', 'line'], fill='red')
        for mins in range(60):
            coords = [mins*60, 65]
            self.tLine.create_text(coords, text=mins,
                                   font=("Courier", 6),
                                   tags=['time', mins, 'txt'])
        self.line = self.tLine.create_line([0, 0, 0, 70], width=2,
                                           tags=['time', 'line', 'actual'], fill='navy')
        #Bindings
        self.tLine.bind("<ButtonPress-1>", self.click)
        self.tLine.bind("<B1-Motion>", self.moveVideo)
        self.tLine.bind("<ButtonPress-3>", self.delVideo)
        self.tLine.bind("<Enter>", self.activarScroll)
        self.tLine.bind("<Leave>", self.desactivarScroll)
        #Threading
        self.is_playing = 0
        self.event = threading.Event()
        self.playing = Thread(target=self.playing_thread, args=(self.event,))
        self.playing.start()

    def playing_thread(self, event):
        while 1:
            event.wait()
            c = self.tLine.coords(self.line)
            self.time.set(c[0]+1*self.scale)
            c[0] = c[2] = float(self.time.get())
            self.tLine.coords(self.line, *c)
            #self.tLine.tag_raise(self.line)
            #if c[0] >= (self.duracion): #Arreglar bucle
                #self.time.set(0)
                #c[0] = c[2] = self.time.get()
                #self.tLine.coords(self.line, *c)
                #self.event.clear()
            for v in self.videos:
                if abs(c[0] - self.videos[v].timeStart) <= 0.8:
                    if not self.player.vw is None:
                        self.path_play_cmd(v)
            self.actualizarTitulo()
            time.sleep(1)

    def botonPlay(self):
        if self.event.isSet():
            #Pausa la reproduccion
            self.event.clear()
            """
            #Activa los binding de nuevo
            self.tLine.bind("<ButtonPress-1>", self.click)
            self.tLine.bind("<B1-Motion>", self.moveVideo)
            self.tLine.bind("<ButtonPress-3>", self.delVideo)
            self.activarScroll()
            """
        else:
            #Empieza o reanuda la reproduccion
            self.event.set()
            """
            #Desactiva los binding de nuevo
            self.tLine.unbind("<ButtonPress-1>")
            self.tLine.unbind("<B1-Motion>")
            self.tLine.unbind("<ButtonPress-3>")
            self.desactivarScroll(event)
            """
            
        c = self.tLine.coords(self.line)
        selec = self.tLine.find_overlapping(*c)
        for item in selec:
            tags = self.tLine.gettags(item)
            if 'video' in tags:
                self.play_cmd()


    def path_play_cmd(self, v):
        self.player.vw.establecer_path(self.videos[v].name)
        self.player.vw.aplicarRecorte(self.videos[v].h, self.videos[v].w)
        self.player.vw.aplicarRotacion()
        self.player.vw.establecer_audio(1) #No va aqui
        #self.player.vw.establecer_path('human') #SOLO DEBUG
        #self.player.vw.establecer_cmd(6)
        self.player.vw.establecer_cmd(4)
        self.player.vw.envio_cmd()
        self.player.vw.establecer_cmd(1)
        self.player.vw.envio_cmd()

    def play_cmd(self):
        if self.player.vw.obtener_estado() == 0:
            self.player.vw.establecer_cmd(1)
        else:
            self.player.vw.establecer_cmd(2)
        self.player.vw.envio_cmd()

    def minimize(self):
        self.borrarAll()
        self.restablecer()

    def chgmode(self):
        pass

    def menuEvents(self):
        pass

    def restablecer(self):
        self.event.clear()
        scale1 = 1/self.scale
        self.scale = 1
        self.tLine.xview("moveto", 0.0)
        #self.tLine.scale(ALL, event.x, 35, scale1, 1)
        self.time.set(0)
        c = self.tLine.coords(self.line)
        c[0] = c[2] = 0
        self.tLine.coords(self.line, *c)

    def actualizarTitulo(self):
        if self.nombreServidor == "":
            self.titulo.set("")
        else:
            self.titulo.set(self.nombreServidor+" -- "+
                            str(self.time.get())+"/"+self.duracion)

    def borrarAll(self):
        self.player = None
        self.setNombreServidor("")
        self.setDuracion("")
        self.actualizarTitulo()
        self.setTime(0)

    def setAll(self, player):
        self.player = player
        self.setNombreServidor(player.alias)
        self.setDuracion(str(player.duracion))
        self.actualizarTitulo()
        self.setTime(0)

    def addvideo(self, name, vid, ext):
        nameID = name + '-' + str(self.id)
        if not self.player is None:
            if not self.player.vw.path.has_key(name):
                print "Video no disponible"
                self.transfervideo(name, ext)
        self.videos[nameID] = copy(vid)
        self.exts[nameID] = copy(ext)
        coords = [0, 0, vid.duration*self.scale, self.h]
        color = random.choice(self.COLORS)
        c = self.tLine.create_rectangle(coords, fill=color,
                                        tags=['video', nameID]) #rectangulo duración
        self.tLine.tag_lower(c)
        self.iconVids[name] = c
        self.id += 1

    def delVideo(self, event):
        self.click(event)
        if len(self.selTags) >= 2:
            nameID = self.selTags[1]
            if self.videos.has_key(nameID):
                self.tLine.delete(nameID)
                self.videos.pop(nameID)
                self.exts.pop(nameID)

    def setNombreServidor(self, n):
        self.nombreServidor = n

    def setDuracion(self, d):
        self.duracion = d

    def setTime(self, t):
        self.time.set(t)

    def activarScroll(self, event):
        #Para Windows
        self.tLine.bind_all("<MouseWheel>", self.scroll)
        #with Linux OS
        self.tLine.bind_all("<Button-4>", self.scroll)
        self.tLine.bind_all("<Button-5>", self.scroll)

    def desactivarScroll(self, event):
        #Para Windows
        self.tLine.unbind_all("<MouseWheel>")
        # with Linux OS
        self.tLine.unbind_all("<Button-4>")
        self.tLine.unbind_all("<Button-5>")

    def scroll(self, event):
        #Posiblemente haya que modificar los timeStart
        if event.num == 5 or event.delta == -120:
            self.tLine.scale(ALL, event.x, 35, 0.9, 1)
            self.scale = self.scale*0.9
        if event.num == 4 or event.delta == 120:
            self.tLine.scale(ALL, event.x, 35, 1.1, 1)
            self.scale = self.scale*1.1
        """ No termina de ser la solucion
        sel = self.tLine.find_withtag('video')
        for video in sel:
            tags = self.tLine.gettags(video)
            if self.videos.has_key(tags[1]):
                c = self.tLine.coords(video)
                self.videos[tags[1]].timeStart(int(c[0]))
        """

    def click(self, event):
        x = self.x = copy(event.x)
        y = self.y = copy(event.y)
        items = self.tLine.find_overlapping(x-1, y-1, x+1, y+1)
        self.selTags = self.tLine.gettags(items)

    def moveVideo(self, event):
        if len(self.selTags) >= 2:
            nameID = self.selTags[1]
            if self.videos.has_key(nameID):
                self.tLine.move(nameID, (-self.x+event.x)/20, 0)
                coords = self.tLine.coords(nameID)
                self.videos[nameID].timeStart = coords[0]

    def transfervideo(self, name, ext):
        out = Video()
        out.path = name + "." + ext.lower()
        print "Enviando: " + name + " a " + self.player.vw.dirIP
        zmqv = myzmqVideo()
        zmqv.connect("tcp://"+ self.player.vw.dirIP +":2324")
        zmqv.send_video(out)
        zmqv.disconnect("tcp://"+ self.player.vw.dirIP +":2324")
        del zmqv

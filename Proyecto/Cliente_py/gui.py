#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: gui.py                                                 *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
import os
from Tkinter import *
from ttk import Combobox, Notebook, Separator, Labelframe, Progressbar
from PIL import Image, ImageTk

class gui(object):
    #ventana = ""
    actualRow = 0
    actualCol = 0
    altura = 0
    anchura = 0

    def __init__(self):
        self.ventana = Tk()
        self.ventana.title("Gestor Video Cluster Rpi")
        if os.name.startswith("nt"):
            self.ventana.state('zoomed')
        else:
            self.ventana.attributes('-zoomed', True)
        #self.ventana.attributes('-fullscreen', True)
        gui.altura = self.ventana.winfo_screenheight()
        gui.anchura = self.ventana.winfo_screenwidth()
        self.note = self.crearCuaderno(self.ventana)
        self.note.grid(row=0, column=0)
        self.pageEstudio = self.addPestana(self.note, "Estudio")
        self.pageVideoWalls = self.addPestana(self.note, "Gest. Video walls")
        #self.pageEvents = self.addPestana(self.note, "Gest. Eventos")
        self.pageInfo = self.addPestana(self.note, "Info Rpis")

    def crearBoton(self, parent, txt, comando, st, nrow, ncol):
        b = Button(parent, text=txt, command=comando, state=st)
        b.grid(row=nrow, column=ncol, padx=5, pady=3)
        return b

    def crearEtiqueta(self, parent, texvar, nrow, ncol, nrowspan=1, ncolpan=1):
        t = Label(parent, text=texvar, anchor=W, justify=LEFT)
        t.grid(row=nrow, column=ncol, rowspan=nrowspan, columnspan=ncolpan)
        return t

    def crearEtiquetaVar(self, parent, texvar, nrow, ncol, nrowspan=1, ncolpan=1):
        t = Label(parent, textvariable=texvar, anchor=W, justify=LEFT)
        t.grid(row=nrow, column=ncol, rowspan=nrowspan, columnspan=ncolpan)
        return t

    def crearFrame(self, parent, nrow, ncol, nrowpan=1, ncolpan=1, tipo=GROOVE):
        f = Frame(parent, relief=tipo, borderwidth=2)
        f.grid(row=nrow, column=ncol, rowspan=nrowpan, columnspan=ncolpan)
        return f

    def crearLabelFrame(self, parent, texvar, nrow, ncol, nrowpan=1, ncolpan=1, tipo=GROOVE):
        lf = Labelframe(parent, text=texvar, relief=tipo, borderwidth=2)
        lf.grid(row=nrow, column=ncol, rowspan=nrowpan, columnspan=ncolpan)
        return lf

    def crearLista(self, parent, valores, nrow, ncol, postcmd=""):
        l = Combobox(parent, values=valores, postcommand=postcmd)
        l.set(valores[0])
        l.state(['readonly'])
        l.configure(width=15)
        l.grid(row=nrow, column=ncol)
        return l

    def crearImagenRAW(self, name, size=None):
        img = Image.open(r'img/'+name)
        img.convert("RGB")
        if size is not None:
            img = img.resize(size, Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(img)
        return photo

    def crearImagen(self, parent, name, nrow, ncol, size=None):
        photo = self.crearImagenRAW(name, size)
        label = Label(parent, image=photo)
        label.grid(row=nrow, column=ncol)
        return photo, label

    def crearMarcoImg(self, parent, name, nrow, ncol, etiqueta):
        frame = self.crearFrame(parent, nrow, ncol, 1, 1, FLAT)
        img = self.crearImagen(frame, name, 0, 0)
        label = self.crearEtiqueta(frame, etiqueta, 1, 0)
        alias = self.crearEntrada(frame, 2, 0)
        estLabel = self.crearEtiqueta(frame, '', 3, 0)
        return (frame, img, label, alias, estLabel)

    def crearImgYTxt(self, parent, name, nrow, ncol, etiqueta, size=None):
        frame = self.crearFrame(parent, nrow, ncol, 1, 1, FLAT)
        img = self.crearImagen(frame, name, 0, 0, size)
        label = self.crearEtiqueta(frame, etiqueta, 1, 0)
        return (frame, img, label)

    def crearEntrada(self, parent, nrow, ncol):
        entry = Entry(parent) #por termirar
        entry.grid(row=nrow, column=ncol)
        return entry

    def crearRadioBot(self, parent, name, var, val, nrow, ncol):
        r = Radiobutton(parent, text=name, variable=var, value=val)
        r.grid(row=nrow, column=ncol)
        return r

    def crearCuaderno(self, parent):
        note = Notebook(parent)
        return note

    def addPestana(self, note, nombre):
        frame = Frame(note)
        note.add(frame, text=nombre)
        return frame

    def crearSeparador(self, parent, _orient, nrow, ncol):
        sep = Separator(parent, orient=_orient)
        if _orient == "horizontal":
            sep.grid(row=nrow, column=ncol, sticky="ew", columnspan=1)
        else:
            sep.grid(row=nrow, column=ncol, sticky="ns", rowspan=1)
        return sep

    def crearCanvas(self, parent, nrow, ncol, nrowpan=1, ncolpan=1):
        c = Canvas(parent)
        c.grid(row=nrow, column=ncol, rowspan=nrowpan, columnspan=ncolpan)
        return c

    def crearImginfo(self, parent, name, nrow, ncol, etiqueta):
        frame = self.crearFrame(parent, nrow, ncol, 1, 1, FLAT)
        img = self.crearImagen(frame, name, 0, 0)
        label = self.crearEtiqueta(frame, etiqueta, 0, 1)
        return (frame, img, label)

    def crearProgreso(self, parent, var, nrow, ncol, nrowspan=1, ncolspan=1):
        p = Progressbar(parent, orient=HORIZONTAL, variable=var, maximum=100, mode='determinate')
        p.grid(row=nrow, column=ncol, rowspan=nrowspan, columnspan=ncolspan, sticky=NSEW)
        return p

    def loop(self):
        self.ventana.mainloop()

#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: servidor.py                                                *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
import time
from myzmq import myzmq

class servidor(myzmq):
    CMDTYPES = {"Play": 1, "Pause": 2, "Stop": 3, "Chg file": 4}
    STTYPES = {"Ocioso": 0, "Abierto": 1, "Playing": 3,
               "Pausado": 4, "Parado": 5, "Terminado": 6, "Error": 7}

    def __init__(self, newServer, _type):
        myzmq.__init__(self, _type)
        self.dirIP = newServer.IP
        self.estado.state = newServer.state
        self.latidoTime = time.time()
        self.path = {}
        self.enVW = 0
        self.posIzq = 0
        self.posArriba = 0
        self.posDcha = 0
        self.posAbajo = 0
        self.rot = 0
        self.comando.prop.audio = 1

    def solicitar_paths(self):
        self.comando.cmd = 5
        self.envio_cmd()
        i = 0
        for p in self.estado.path:
            vid = str(p)
            vid = vid.split('.')[0]
            self.path[vid] = i
            i += 1

    def aplicarRecorte(self, height, width, enable=0):
        if enable:
            if self.posIzq or self.posArriba or self.posDcha or self.posAbajo:
                self.comando.prop.izq = int((width * self.posIzq)/100)
                self.comando.prop.arriba = int((height * self.posArriba)/100)
                self.comando.prop.dcha = int(width - (width * self.posDcha)/100)
                self.comando.prop.abajo = int(height - (height * self.posAbajo)/100)
        else:
            self.comando.prop.izq = 0
            self.comando.prop.arriba = 0
            self.comando.prop.dcha = 0
            self.comando.prop.abajo = 0

    def aplicarRotacion(self, EnableRot=0):
        if self.rot:
            self.comando.prop.rot = EnableRot*self.rot

    def establecer_cmd(self, _cmd):
        self.comando.cmd = _cmd

    def establecer_audio(self, _audio):
        self.comando.prop.audio = _audio

    def establecer_path(self, video):
        if self.path.has_key(video):
            self.comando.path = self.path[video]

    def obtener_estado(self):
        return self.estado.state

#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: gestorzmq.py                                               *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""

from Tkinter import *
from copy import copy
from servidor import servidor
from studio import Studio
from inforpi import Inforpi
import zmq

class Gestorzmq(object):
    CMDTYPES = {"Play": 1, "Pause": 2, "Stop": 3, "Carga file": 4}
    STTYPES = {"Ocioso": 0, "Abierto": 1, "Playing": 3,
               "Pausado": 4, "Parado": 5, "Terminado": 6, "Error": 7}
    AUDIO = {"Con audio": 1, "Silencioso": 0}

    def __init__(self, _ventana):
        self.dicServidores = {}
        self.alias = {}
        #Declaracion Studio
        self.ventana = _ventana
        self.studio = Studio(self.ventana.pageEstudio, _ventana, self)
        self.studio.grid()
        #Declaracion Info rpi
        finfo = self.ventana.pageInfo
        self.info = Inforpi(finfo, self.ventana, self)
        self.info.grid()

    def addServidor(self, _servidor):
        s = servidor(_servidor, zmq.REQ)
        if not self.dicServidores.has_key(s.dirIP):
            s.connect()
            s.conectado = 1
            s.solicitar_paths()
            self.dicServidores[s.dirIP] = s
            num = len(self.dicServidores)
            self.info.addEquipo(s.dirIP, num)
            self.crearPlayerServidor(s.dirIP)

    def crearPlayerServidor(self, IP):
        server = copy(IP)
        self.studio.selAlias = copy(IP)
        if self.alias.has_key(server):
            server = self.alias[server]
            self.studio.selAlias = server
        self.studio.selServidor = self.dicServidores[server]
        self.studio.configNewPlayer()

    def servidorIncluido(self, IP):
        server = copy(IP)
        if self.alias.has_key(server):
            server = self.alias[server]
        return self.dicServidores.has_key(server)

    def addVideos(self, videos, exts):
        print "Videos: ", videos.keys()
        self.studio.chgVideos(videos, exts)

    def LatidoServidor(self, estado, Time):
        server = copy(estado.IP)
        if self.alias.has_key(estado.IP):
            server = self.alias[estado.IP]
        self.dicServidores[server].latidoTime = Time
        st = self.dicServidores[server].estado.state = estado.state
        txt = self.STTYPES.keys()[self.STTYPES.values().index(st)]
        self.info.chgEstado(server, txt)
        #self.studio.chgEstado(server, actualTime) #TODO:Implementar

    def Watchdog(self, now):
        caidos = []
        for server in self.dicServidores:
            s = self.dicServidores[server]
            if (now - s.latidoTime) > 8:
                caidos.append(server)
        for server in caidos:
            if self.alias.has_key(server):
                server = self.alias[server]
            if isinstance(self.dicServidores[server], servidor):
                self.info.destroyInfoServer(server)
            self.studio.destroyServer(server)
            self.dicServidores.pop(server)

    def cambiarAlias(self, server, alias):
        self.alias[server] = alias
        self.studio.chgAlias(server, alias)

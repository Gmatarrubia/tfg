#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: myzmq.py                                                 *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
from mensaje_pb2 import Command
from mensaje_pb2 import State
import zmq

class myzmq(object):
    context = zmq.Context(1)

    def __init__(self, sockType):
        self.socket = self.context.socket(sockType)
        self.comando = Command()
        self.estado = State()
        self.conectado = 0
        self.dirIP = ""

    def initServer(self, dirIP):
        #Se crea el Socket y se define puerto
        self.socket.bind(dirIP)
        print ("Iniciado servidor. Puerto de escucha: %s" % (dirIP.split(':')[2]))

    def connect(self):
        #Se crea el Socket y se conecta a la dirIP
        self.socket.connect("tcp://" + self.dirIP + ":2323")
        print ("Conectado a servidor %s" % (self.dirIP))

    def envio_cmd(self):
        print ("Enviando comando: " + str(self.comando.cmd))
        print self.comando.prop.izq, " - ", self.comando.prop.dcha
        print self.comando.prop.arriba, " | ", self.comando.prop.abajo
        print"Rot:", self.comando.prop.rot
        print "Audio:", self.comando.prop.audio
        self.socket.send(self.comando.SerializeToString())
        #  Get the reply.
        data = self.socket.recv()
        self.estado.ParseFromString(data)
        print("Recibida respuesta: " +str(self.estado.state))

    def recibir_state(self):
        data = self.socket.recv()
        self.estado.ParseFromString(data)
        self.socket.send(data)
        return (self.estado.IP, self.estado.state)

    def disconnect(self, dirIP):
        #Se desconecta el Socket de la dirIP
        self.socket.disconnect(dirIP)
        self.dirIP = ""
        print ("Desconectado: %s" % (dirIP))

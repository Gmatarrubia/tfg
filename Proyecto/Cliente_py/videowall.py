#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: videoWall.py                                               *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
import time
from servidor import servidor

class VideoWall(object):
    CMDTYPES = {"Play": 1, "Pause": 2, "Stop": 3, "Chg file": 4}
    STTYPES = {"Ocioso": 0, "Abierto": 1, "Playing": 3,
               "Pausado": 4, "Parado": 5, "Terminado": 6, "Error": 7}
    enVW = 1 #Evita que se puedan hacer VW de VW
    def __init__(self):
        self.dicServidores = {}

    def addServidor(self, server):
        self.dicServidores[server.dirIP] = server

    def envio_cmd(self):
        for server in self.dicServidores:
            self.dicServidores[server].envio_cmd()

    @property
    def latidoTime(self):
        now = time.time()
        for server in self.dicServidores:
            s = self.dicServidores[server]
            if (now - s.latidoTime) > 8:
                return 0
        return time.time()

    @property
    def path(self):
        """ Los paths del videoWall son los que comparten todos las rpi
        *   que lo forman. """
        paths = {}
        eliminaciones = []
        referencia = 1
        for server in self.dicServidores:
            if referencia == 1:
                paths = self.dicServidores[server].path.copy()
                referencia += 1
            else:
                for p in paths:
                    if not p in self.dicServidores[server].path:
                        eliminaciones.append(p)
                for e in eliminaciones:
                    paths.pop(e)
        return paths

    def aplicarRecorte(self, height, width):
        for server in self.dicServidores:
            self.dicServidores[server].aplicarRecorte(height, width, 1)

    def aplicarRotacion(self, EnableRot=0):
        for server in self.dicServidores:
            self.dicServidores[server].aplicarRotacion(1)

    def establecer_cmd(self, comando):
        for server in self.dicServidores:
            self.dicServidores[server].comando.cmd = comando

    def establecer_path(self, video):
        if self.path.has_key(video):
            for server in self.dicServidores:
                self.dicServidores[server].comando.path = self.dicServidores[server].path[video]

    def establecer_audio(self, _audio):
        #Para que solo haya 1 dispositivo con audio o todo silencio
        primero = 0
        for server in self.dicServidores:
            if primero == 0:
                self.dicServidores[server].establecer_audio(_audio)
                primero +=1
            else:
                self.dicServidores[server].establecer_audio(0)

    def obtener_estado(self):
        for server in self.dicServidores:
            return self.dicServidores[server].estado.state
    """@property
    def comando(self):
        for server in self.dicServidores:
            return self.dicServidores[server].comando.cmd
        return '0'
    @comando.setter
    def comando(self, comando):
        for server in self.dicServidores:
            self.dicServidores[server].comando.cmd = comando"""

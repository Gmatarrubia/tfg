#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: main.py                                                    *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
import zmq
import time
from myzmq import myzmq
from mensaje_pb2 import Video

class myzmqVideo(myzmq):

    def __init__(self):
        myzmq.__init__(self, zmq.PAIR)

    def connect(self, dirIP):
        #Se crea el Socket y se define puerto
        self.dirIP = dirIP
        self.socket.connect(dirIP)

    def send_video(self, video):
        in_video = Video()
        path = "./videos/" + video.path
        self.socket.send(video.SerializeToString())
        vid = open(path, "rb")
        data = self.socket.recv()
        in_video.ParseFromString(data)
        if in_video.path != video.path:
            return
        stream = True
        # Start reading in the file
        while stream:
            stream = vid.read(512)
            if stream:
                # If the stream has more to send then send more
                self.socket.send(stream, zmq.SNDMORE)
            else:
                # Finish it off
                self.socket.send(stream)
            #time.sleep(0.001)
